<a href="http://isa.uniovi.es/GSDPI"><img width=90% src="http://isa.uniovi.es//~idiaz/logos/header_gsdpi_somuniovi.png" alt=""></a>

# SOMUniOvi: Self Organizing Map
SOMUniOvi is a simple minimalistic library to train and visualize self-organizing maps (SOM) in python. 

## Installation and requirements
- Just ensure `somuniovi.py` is accesible in the python module search path
- The library and the included examples use `numpy` and `matplotlib` 

## Getting Started
To get started head to the `ipynb` examples provided

- `Visualizing MNIST with SOM.ipynb`
- `Vibration and current frequency visual analysis using SOM.ipynb`
- `SOM toy example.ipynb`

## Examples of use
Some code fragments, to have a first glance of library usage. See the `ipynb` examples for details.


```python
import somuniovi as som

# Create a 50x50 SOM model ...
a = som.SOM(p,dims=(50,50),labels=['var 1','var 2','var 3'])

# PCA (linear) initialization ...
a.pca_init(p)

# Train the SOM ...
a.train(p,N_initial=2,N_final=1,epochs=10,verbose=True)

# plot the SOM component planes after training ...
plt.figure(figsize=(15,7))
ax = a.planes(vmin=-6,vmax=6)

# get projections on the 2D lattice ...
pr = a.fproj(p_test)[0]

# Compute and visualize *u-matrix* ...
d = a.somdist()
fig = plt.figure(figsize=(20,15))
som.planes(d,a.gi,a.dims,labels=['SOM distance map'])
```
